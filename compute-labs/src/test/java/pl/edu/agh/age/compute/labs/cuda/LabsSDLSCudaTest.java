/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import static jcuda.driver.JCudaDriver.cuCtxCreate;
import static jcuda.driver.JCudaDriver.cuCtxSynchronize;
import static jcuda.driver.JCudaDriver.cuDeviceGet;
import static jcuda.driver.JCudaDriver.cuInit;
import static jcuda.driver.JCudaDriver.cuLaunchKernel;
import static jcuda.driver.JCudaDriver.cuMemAlloc;
import static jcuda.driver.JCudaDriver.cuMemFree;
import static jcuda.driver.JCudaDriver.cuMemcpyDtoH;
import static jcuda.driver.JCudaDriver.cuMemcpyHtoD;
import static jcuda.driver.JCudaDriver.cuModuleGetFunction;
import static jcuda.driver.JCudaDriver.cuModuleLoad;

import pl.edu.agh.age.compute.labs.evaluator.LabsEnergy;
import pl.edu.agh.age.compute.labs.solution.LabsSolution;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import io.vavr.collection.Array;
import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUcontext;
import jcuda.driver.CUdevice;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import jcuda.driver.JCudaDriver;

public class LabsSDLSCudaTest {

	/**
	 * @param args
	 * 		the command line arguments
	 */
	public static void main(String args[]) throws IOException {
		// Enable exceptions and omit all subsequent error checks
		JCudaDriver.setExceptionsEnabled(true);

		// Create the PTX file by calling the NVCC
		String ptxFileName = preparePtxFile("src/main/resources/pl/edu/agh/age/labs/cuda/kernel.cu");

		// Initialize the driver and create a context for the first device.
		cuInit(0);
		CUdevice device = new CUdevice();
		cuDeviceGet(device, 0);
		CUcontext context = new CUcontext();
		cuCtxCreate(context, 0, device);

		// Load the ptx file.
		CUmodule module = new CUmodule();
		cuModuleLoad(module, ptxFileName);

		// Obtain a function pointer to the "slds" function.
		CUfunction function = new CUfunction();
		cuModuleGetFunction(function, module, "kernel");

		int lengthOfSequence = 50;
		int numberOfBlock = 25;
		int numberOfThreads = 64;
		int numberOfRepeating = 40;

		long startTimeWithCopy = System.nanoTime();

		// Allocate and fill the host input data <--- to co ma byc wczesniej
		short hostInputA[] = new short[numberOfThreads * numberOfBlock];
		CUdeviceptr deviceInputA = new CUdeviceptr();
		cuMemAlloc(deviceInputA, numberOfThreads * numberOfBlock * Sizeof.SHORT);

		CUdeviceptr deviceNumberOfRepeating = new CUdeviceptr();
		cuMemAlloc(deviceNumberOfRepeating, Sizeof.INT);

		// Allocate device output memory
		CUdeviceptr devResult = new CUdeviceptr();
		cuMemAlloc(devResult, numberOfBlock * Sizeof.INT);


		cuMemcpyHtoD(deviceNumberOfRepeating, Pointer.to(new int[] {numberOfRepeating}), Sizeof.INT);


		//TUTAJ UZUPELNIANIE CIAGU WEJSCIOWEGO (NAPISANE NA SZYBKO :) ))

		int lOfS = 0;
		for (int i = 0; i < numberOfThreads * numberOfBlock; i++) {
			if (lOfS < lengthOfSequence) {
				hostInputA[i] = (short)1;
			} else {
				hostInputA[i] = (short)0;
			}
			lOfS++;
			if (lOfS == numberOfThreads) { lOfS = 0; }
		}

		// Allocate the device input data, and copy the
		// host input data to the device

		/// CIAG WEJSCIOWY MOZESZ KOPIOWAC ZA KAZDYM RAZEM

		cuMemcpyHtoD(deviceInputA, Pointer.to(hostInputA), numberOfThreads * numberOfBlock * Sizeof.SHORT);


		Pointer kernelParameters = Pointer.to(Pointer.to(deviceInputA), Pointer.to(devResult),
		                                      Pointer.to(deviceNumberOfRepeating));

		long startTime = System.nanoTime();
		// Call the kernel function.
		cuLaunchKernel(function, numberOfBlock, 1, 1,      // Grid dimension
		               numberOfThreads, 1, 1,      // Block dimension
		               0, null,               // Shared memory size and stream
		               kernelParameters, null // Kernel- and extra parameters
		              );
		cuCtxSynchronize();

		long endTime = System.nanoTime();
		long duration = (endTime - startTime);

		System.out.println("duration only on kernel is [" + duration / 1000000 + "]");

		// Allocate host output memory and copy the device output
		// to the host.
		int energy[] = new int[numberOfBlock];
		cuMemcpyDtoH(Pointer.to(energy), devResult, numberOfBlock * Sizeof.INT);

		short sequence[] = new short[lengthOfSequence * numberOfBlock]; // FIXME: lengthOfSequence or numberOfThreads?
		cuMemcpyDtoH(Pointer.to(sequence), deviceInputA,
		             lengthOfSequence * numberOfBlock * Sizeof.SHORT); // FIXME: lengthOfSequence or numberOfThreads?


		long endTimeWithCopy = System.nanoTime();
		long durationWithCopy = (endTimeWithCopy - startTimeWithCopy);

		System.out.println("duration all algorithm [" + durationWithCopy / 1000000 + "]");

		// Verify the result
		verifyGPUResults(energy, sequence, lengthOfSequence,
		                 lengthOfSequence); // FIXME: lengthOfSequence or numberOfThreads?


		// Clean up.
		cuMemFree(deviceInputA);
		cuMemFree(devResult);
		cuMemFree(deviceNumberOfRepeating);

	}


	private static boolean verifyGPUResults(int[] energy, short[] sequence, int numberOfThreads, int lengthOfSequence) {
		for (int i = 0; i < energy.length; i++) {
			short[] solution = Arrays.copyOfRange(sequence, i * numberOfThreads,
			                                      i * numberOfThreads + lengthOfSequence);
			LabsEnergy referenceEnergy = new LabsEnergy(new LabsSolution(Array.ofAll(solution)));
			if (energy[i] == referenceEnergy.value()) {
				System.out.println("PASSED");
			} else {
				System.err.println("WRONG");
			}
		}

		return true;
	}


	/**
	 * The extension of the given file name is replaced with "ptx".
	 * If the file with the resulting name does not exist, it is
	 * compiled from the given file using NVCC. The name of the
	 * PTX file is returned.
	 *
	 * @param cuFileName
	 * 		The name of the .CU file
	 *
	 * @return The name of the PTX file
	 *
	 * @throws IOException
	 * 		If an I/O error occurs
	 */
	private static String preparePtxFile(String cuFileName) throws IOException {
		int endIndex = cuFileName.lastIndexOf('.');
		if (endIndex == -1) {
			endIndex = cuFileName.length() - 1;
		}
		String ptxFileName = cuFileName.substring(0, endIndex + 1) + "ptx";

		// if (ptxFile.exists())
		//{
		// return ptxFileName;
		//}

		File cuFile = new File(cuFileName);
		if (!cuFile.exists()) {
			throw new IOException("Input file not found: " + cuFileName);
		}
		String modelString = "-m" + System.getProperty("sun.arch.data.model");
		System.out.println("model stirng is " + modelString);
		String command = "nvcc " + modelString + " -ptx " + cuFile.getPath() + " -o " + ptxFileName;

		System.out.println("Executing\n" + command);
		Process process = Runtime.getRuntime().exec(command);

		String errorMessage = new String(toByteArray(process.getErrorStream()));
		String outputMessage = new String(toByteArray(process.getInputStream()));
		int exitValue = 0;
		try {
			exitValue = process.waitFor();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new IOException("Interrupted while waiting for nvcc output", e);
		}

		if (exitValue != 0) {
			System.out.println("nvcc process exitValue " + exitValue);
			System.out.println("errorMessage:\n" + errorMessage);
			System.out.println("outputMessage:\n" + outputMessage);
			throw new IOException("Could not create .ptx file: " + errorMessage);
		}

		System.out.println("Finished creating PTX file");
		return ptxFileName;
	}

	/**
	 * Fully reads the given InputStream and returns it as a byte array
	 *
	 * @param inputStream
	 * 		The input stream to read
	 *
	 * @return The byte array containing the data from the input stream
	 *
	 * @throws IOException
	 * 		If an I/O error occurs
	 */
	private static byte[] toByteArray(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte buffer[] = new byte[8192];
		while (true) {
			int read = inputStream.read(buffer);
			if (read == -1) {
				break;
			}
			baos.write(buffer, 0, read);
		}
		return baos.toByteArray();
	}
}
