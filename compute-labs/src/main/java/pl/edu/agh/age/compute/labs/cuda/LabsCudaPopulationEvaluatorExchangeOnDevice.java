/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import pl.edu.agh.age.compute.api.ThreadPool;
import pl.edu.agh.age.compute.stream.emas.EmasAgent;
import pl.edu.agh.age.compute.stream.problem.EvaluatorCounter;

import one.util.streamex.StreamEx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PreDestroy;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

public final class LabsCudaPopulationEvaluatorExchangeOnDevice {

	private static final Logger log = LoggerFactory.getLogger(LabsCudaPopulationEvaluatorExchangeOnDevice.class);

	private final int numberOfRepeating;

	private final EvaluatorCounter counter;

	private final boolean useCache;

	private final int cacheSize;

	private final ReadWriteLock lock = new ReentrantReadWriteLock();

	private boolean computing = false;

	private List<ComparableAgent> cache;

	private final List<ComparableAgent> computed;

	private final DeviceExecutor executor;

	public LabsCudaPopulationEvaluatorExchangeOnDevice(final int lengthOfSequence, final int numberOfThreads,
	                                                   final int numberOfRepeating,
	                                                   final boolean modifyEvaluatedSolution, final boolean useCache,
	                                                   final int cacheSize, final String cudaFilePath,
	                                                   final EvaluatorCounter counter, final int awaitLimitInMicroS,
	                                                   final ThreadPool threadPool) {

		this.numberOfRepeating = numberOfRepeating;
		this.useCache = useCache;
		this.cacheSize = cacheSize;
		this.counter = counter;
		cache = new ArrayList<>(cacheSize);
		computed = new ArrayList<>(cacheSize);

		executor = new DeviceExecutor(cudaFilePath, modifyEvaluatedSolution, lengthOfSequence, numberOfThreads,
		                              numberOfRepeating);

		log.debug("Creating schedule...");
		threadPool.scheduleWithFixedDelay(this::evaluationStep, 10, awaitLimitInMicroS, TimeUnit.MICROSECONDS);
	}

	@PreDestroy public void destroy() {
		executor.cleanUp();
	}

	public Set<EmasAgent> evaluate(final int origin, final Set<EmasAgent> population) {
		final java.util.Set<ComparableAgent> converted = population.map(agent -> new ComparableAgent(agent, origin))
		                                                           .toJavaSet();
		lock.writeLock().lock();
		final Map<Boolean, List<ComparableAgent>> partition;
		final List<ComparableAgent> workplaceAgents;
		try {
			cache.addAll(converted);
			log.debug("Now we have {} agents from {}", StreamEx.of(cache).filter(a -> a.workplace == origin).count(),
			          origin);

			//evaluationStep();

			partition = StreamEx.of(computed).partitioningBy(a -> a.workplace == origin);
			workplaceAgents = partition.get(true);
			computed.removeAll(workplaceAgents);
		} finally {
			lock.writeLock().unlock();
		}

		return HashSet.ofAll(workplaceAgents).map(ca -> ca.agent);
	}


	private void evaluationStep() {
		lock.writeLock().lock();
		final List<ComparableAgent> cacheCopy;
		final int numberOfBlocks;
		try {
			if (computing) {
				log.debug("Computation is ongoing");
				return;
			}

			numberOfBlocks = cache.size();
			log.debug("Evaluating population with {} individuals", numberOfBlocks);

			if (numberOfBlocks == 0) {
				log.debug("Empty cache");
				return;
			}

			// Allocate and fill the host input data

			if (useCache && (numberOfBlocks < cacheSize)) {
				log.debug("Cache in use but still not filled - need {} more agents", cacheSize - numberOfBlocks);
				return;
			}

			log.debug("Ok - offloading to GPU");

			computing = true;
			cacheCopy = cache;
			cache = new ArrayList<>(cacheSize);
		} finally {
			lock.writeLock().unlock();
		}

		final List<ComparableAgent> newPopulation = executor.execute(cacheCopy);

		lock.writeLock().lock();
		try {
			computing = false;
			computed.addAll(newPopulation);

			//log.debug("Merged {}", computed);

			// increment a counter
			counter.increment(numberOfBlocks * numberOfRepeating);
		} finally {
			lock.writeLock().unlock();
		}
	}

}
