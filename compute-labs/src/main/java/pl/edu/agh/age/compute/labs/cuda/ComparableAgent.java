/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import pl.edu.agh.age.compute.stream.emas.EmasAgent;

final class ComparableAgent implements Comparable<ComparableAgent> {

	final EmasAgent agent;

	final int workplace;

	ComparableAgent(final EmasAgent agent, final int workplace) {
		this.agent = agent;
		this.workplace = workplace;
	}

	@Override public int compareTo(final ComparableAgent o) {
		return agent.id.compareTo(o.agent.id);
	}

	@Override public boolean equals(Object o) {
		if (!(o instanceof ComparableAgent)) { return false; }
		return compareTo((ComparableAgent)o) == 0;
	}

	@Override public String toString() {
		return agent.toString();
	}
}
