/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import pl.edu.agh.age.compute.api.ThreadPool;
import pl.edu.agh.age.compute.stream.emas.EmasAgent;
import pl.edu.agh.age.compute.stream.problem.EvaluatorCounter;

import one.util.streamex.StreamEx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PreDestroy;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

public final class LabsCudaPopulationEvaluatorSyncOnDevice {

	private static final Logger log = LoggerFactory.getLogger(LabsCudaPopulationEvaluatorSyncOnDevice.class);

	private final int numberOfRepeating;

	private final EvaluatorCounter counter;

	private final int cacheSize;

	private final int awaitLimitInMicroS;

	private final DeviceExecutor executor;

	private final ReadWriteLock lock = new ReentrantReadWriteLock();

	private final Condition emptied = lock.writeLock().newCondition();

	private final Condition isComputed = lock.writeLock().newCondition();

	private final Condition added = lock.writeLock().newCondition();

	private List<ComparableAgent> cache;

	private List<ComparableAgent> computed;

	public LabsCudaPopulationEvaluatorSyncOnDevice(final int lengthOfSequence, final int numberOfThreads,
	                                               final int numberOfRepeating, final boolean modifyEvaluatedSolution,
	                                               final boolean useCache, final int cacheSize,
	                                               final String cudaFilePath, final EvaluatorCounter counter,
	                                               final int awaitLimitInMicroS, final ThreadPool threadPool) {
		this.numberOfRepeating = numberOfRepeating;
		this.cacheSize = cacheSize;
		this.counter = counter;
		this.awaitLimitInMicroS = awaitLimitInMicroS;
		cache = new ArrayList<>(cacheSize);
		computed = new ArrayList<>(cacheSize);

		executor = new DeviceExecutor(cudaFilePath, modifyEvaluatedSolution, lengthOfSequence, numberOfThreads,
		                              numberOfRepeating);

		log.debug("Creating schedule...");
		threadPool.scheduleWithFixedDelay(this::evaluationStep, 10, 1, TimeUnit.MICROSECONDS);
	}

	@PreDestroy public void destroy() {
		executor.cleanUp();
	}

	public Set<EmasAgent> evaluate(final int origin, final Set<EmasAgent> population) throws InterruptedException {
		List<ComparableAgent> converted = population.map(agent -> new ComparableAgent(agent, origin)).toJavaList();
		Set<ComparableAgent> outputPopulation = HashSet.empty();

		lock.writeLock().lock();
		try {
			while (!converted.isEmpty()) {
				// If there is no place in cache – wait until emptied
				if ((cacheSize - cache.size()) == 0) {
					log.debug("[{}] Awaiting emptied cache", origin);
					emptied.await();
				}
				final int freePlaces = cacheSize - cache.size();
				final List<ComparableAgent> toSend = StreamEx.of(converted).limit(freePlaces).toList();
				converted = StreamEx.of(converted).skip(freePlaces).toList();
				cache.addAll(toSend);
				added.signalAll();
			}

			while (outputPopulation.length() < population.length()) {
				log.debug("[{}] Awaiting results", origin);
				isComputed.await();
				final Map<Boolean, List<ComparableAgent>> partition = StreamEx.of(cache)
				                                                              .partitioningBy(
					                                                              a -> a.workplace == origin);
				cache.removeAll(partition.get(true));
				outputPopulation = outputPopulation.addAll(partition.get(true));
			}
		} finally {
			lock.writeLock().unlock();
		}

		final Set<EmasAgent> newPopulation = outputPopulation.map(ca -> ca.agent);
		if (population.length() != newPopulation.length()) {
			log.warn("[{}] Different input and output population sizes: {} != {}", origin, population.length(),
			         newPopulation.length());
		}
		return newPopulation;
	}

	private void evaluationStep() {
		lock.writeLock().lock();
		final int numberOfBlocks;
		final List<ComparableAgent> cacheCopy;
		try {
			boolean notElapsed = true;
			while (notElapsed && (cache.size() < cacheSize)) {
				notElapsed = added.await(awaitLimitInMicroS, TimeUnit.MICROSECONDS);
			}
			/*if (notElapsed) {
				if (cacheSet.size() < cacheSize) {
					log.warn("Returned from condition correctly but cache not filled? {}", cacheSet.size());
				}
			} else {
				log.debug("Evaluating due to too long wait");
			}*/

			numberOfBlocks = cache.size();
			if (numberOfBlocks == 0) {
				log.debug("Empty cache");
				return;
			}

			log.debug("Evaluating population with {} individuals", numberOfBlocks);

			cacheCopy = cache;
			cache = new ArrayList<>(cacheSize);
			emptied.signalAll();
		} catch (final InterruptedException e) {
			log.debug("Interrupted", e);
			Thread.currentThread().interrupt();
			return;
		} finally {
			lock.writeLock().unlock();
		}

		final List<ComparableAgent> newPopulation = executor.execute(cacheCopy);

		lock.writeLock().lock();
		try {
			computed.addAll(newPopulation);
			counter.increment(numberOfBlocks * numberOfRepeating);
			isComputed.signalAll();
		} finally {
			lock.writeLock().unlock();
		}
	}

}
