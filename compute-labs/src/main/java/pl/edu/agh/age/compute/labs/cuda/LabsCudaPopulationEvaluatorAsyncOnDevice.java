/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import pl.edu.agh.age.compute.api.ThreadPool;
import pl.edu.agh.age.compute.stream.emas.EmasAgent;
import pl.edu.agh.age.compute.stream.problem.EvaluatorCounter;

import com.google.common.collect.Maps;
import com.google.common.collect.Queues;

import one.util.streamex.StreamEx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import javax.annotation.PreDestroy;

import io.vavr.collection.Set;

@SuppressWarnings({"Duplicates", "UnstableApiUsage"})
public final class LabsCudaPopulationEvaluatorAsyncOnDevice {

	private static final Logger log = LoggerFactory.getLogger(LabsCudaPopulationEvaluatorAsyncOnDevice.class);

	private final int numberOfRepeating;

	private final EvaluatorCounter counter;

	private final boolean useCache;

	private final int cacheSize;

	private final DeviceExecutor executor;

	private final AtomicBoolean computing = new AtomicBoolean(false);

	private final ArrayBlockingQueue<ComparableAgent> cacheSet;

	private final ConcurrentMap<Integer, Consumer<List<EmasAgent>>> callbacks = Maps.newConcurrentMap();

	public LabsCudaPopulationEvaluatorAsyncOnDevice(final int lengthOfSequence, final int numberOfThreads,
	                                                final int numberOfRepeating, final boolean modifyEvaluatedSolution,
	                                                final boolean useCache, final int cacheSize,
	                                                final String cudaFilePath, final EvaluatorCounter counter,
	                                                final int awaitLimitInMicroS, final ThreadPool threadPool) {
		this.numberOfRepeating = numberOfRepeating;
		this.useCache = useCache;
		this.cacheSize = cacheSize;
		this.counter = counter;

		cacheSet = Queues.newArrayBlockingQueue(cacheSize);

		executor = new DeviceExecutor(cudaFilePath, modifyEvaluatedSolution, lengthOfSequence, numberOfThreads,
		                              numberOfRepeating);

		log.debug("Creating schedule...");
		threadPool.scheduleWithFixedDelay(this::evaluationStep, 10, awaitLimitInMicroS, TimeUnit.MICROSECONDS);
	}

	@PreDestroy public void destroy() {
		executor.cleanUp();
	}

	public void evaluate(final int origin, final Set<EmasAgent> population) {
		final java.util.Set<ComparableAgent> converted = population.map(agent -> new ComparableAgent(agent, origin))
		                                                           .toJavaSet();
		converted.forEach(a -> {
			try {
				cacheSet.put(a);
			} catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		});
	}

	public void register(final int origin, final Consumer<List<EmasAgent>> callback) {
		callbacks.put(origin, callback);
	}

	private void evaluationStep() {
		if (!computing.compareAndSet(false, true)) {
			log.debug("Computation is ongoing");
			return;
		}

		final int numberOfBlocks = cacheSet.size();
		log.debug("Evaluating population with {} individuals", numberOfBlocks);

		if (numberOfBlocks == 0) {
			log.debug("Empty cache");
			computing.set(false);
			return;
		}

		// Allocate and fill the host input data

		if (useCache && (numberOfBlocks < cacheSize)) {
			log.debug("Cache in use but still not filled - need {} more agents", cacheSize - numberOfBlocks);
			computing.set(false);
			return;
		}

		log.debug("Ok - offloading to GPU");

		final List<ComparableAgent> cache = new ArrayList<>(cacheSet.size());
		cacheSet.drainTo(cache);

		final List<ComparableAgent> newPopulation = executor.execute(cache);

		final Map<Integer, List<ComparableAgent>> map = StreamEx.of(newPopulation).groupingBy(ag -> ag.workplace);

		map.forEach((w, a) -> callbacks.get(w).accept(StreamEx.of(a).map(ag -> ag.agent).toList()));
		counter.increment(numberOfBlocks * numberOfRepeating);
		computing.set(false);
	}

}
