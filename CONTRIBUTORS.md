Contributors are ordered by the first contribution.

Contributors to AgE 3:

- [Kamil Piętak](https://gitlab.com/kpietak) <kpietak@agh.edu.pl>
- Marek Kisiel-Dorohinicki <doroh@agh.edu.pl>
- [Łukasz Faber](https://gitlab.com/Nnidyu) <faber@agh.edu.pl>
- [Kamil Kuchta](https://gitlab.com/KamilKuchta) <kuchtakamil@gmail.com>
- [Grzegorz Kaczmarczyk](https://gitlab.com/kaczmarczyk) <grzegorz.kaczmarczyk@mail.com> 
- [Andrzej Dymara](https://gitlab.com/dymara) <andrzejdymara@gmail.com>


Contributors to the earlier versions (before AgE 3) and initial authors of various code fragments imported from old versions include:

- Paweł Kędzior
- Tomasz Kmiecik
- Krzysztof Sikora
- Adam Woś
- Daniel Krzywicki
- Kamil Piętak
- Łukasz Faber
- ...and others
