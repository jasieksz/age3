#!/bin/bash

set -e

readonly shell_binary=$1

./gradlew age3-examples:build
cp age3-examples/build/libs/age3-examples-0.7-SNAPSHOT.jar ./lib/

readonly examples_list="SimpleLongRunning
Simple
SimpleLongRunningWithError
SimpleWithQuery
SimpleWithBroadcastCommunication
SimpleWithUnicastCommunication
SimpleWithProperty"

for i in $examples_list; do
	tests/test-example.sh $shell_binary $i
done

