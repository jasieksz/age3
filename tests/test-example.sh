#!/bin/bash

set -e

readonly shell_binary=$1
readonly example=$2

cat << EOF > comp-config
test.executeExample({ example: '$example' })
computation.waitUntilFinished()
EOF

java -jar $shell_binary standalone ./comp-config

