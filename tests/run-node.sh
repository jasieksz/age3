#!/bin/bash

set -e

./gradlew age3-core:shadowJar

java -jar age3-core/build/libs/age3-core.jar
